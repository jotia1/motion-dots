"""An python implementation of a QuadTree based off Steven Lambert's java tutorial
http://gamedevelopment.tutsplus.com/tutorials/quick-tip-use-quadtrees-to-detect-likely-collisions-in-2d-space--gamedev-374

and the stack overflow answer:
http://stackoverflow.com/questions/17856540/pygame-efficient-collision-handling-for-rpg-game

"""
MAX_OBJECTS = 5
MAX_LEVELS = 20


class QuadTree(object):
    """Implementation of a quad tree to divide up a region"""
    def __init__(self, level, bounds, min_size):
        """Create a quad tree root or node. 
        
        bounds: a tuple defining a rectangle in form (x, y, width, height)
                where x,y is the top left corner
        min_size: the minimum size for a node
        """
        self.bounds = bounds
        self.level = level
        self.nw = self.ne = self.se = self.sw = None
        self.envos = []
        self.min_size = min_size
        self.leaf = True

    def clear(self):
        """Reset this node to empty"""
        for node in self.nodes:
            node.clear()
            self.envos = []

    def get_envos(self):
        return self.envos

    def split(self):
        self.leaf = False
        hwidth = self.bounds[2]/2
        hheight = self.bounds[3]/2
        x = self.bounds[0]
        y = self.bounds[1]

        #Create the four nodes
        self.ne = QuadTree(self.level+1,
                (x + hwidth, y, hwidth, hheight), self.min_size)
        self.nw = QuadTree(self.level+1,
                (x, y, hwidth, hheight), self.min_size)
        self.sw = QuadTree(self.level+1,
                (x, y + hheight, hwidth, hheight), self.min_size)
        self.se = QuadTree(self.level+1,
                (x + hwidth, y + hheight, hwidth, hheight), self.min_size)

    def _which_quad(self, envo):
        """Return a str of which quad this EnvObject is in"""
        midx = self.bounds[0] + self.bounds[2]/2
        midy = self.bounds[1] + self.bounds[3]/2
        #Check if it can fit in top/bottom half of current quadrant
        x, y = envo.get_pos()
        if y < midy: #If in top half
            result = 'nw' if x < midx else 'ne'
        else:
            result = 'sw' if x < midx else 'se'
        return result

    def _add_to_quad(self, envo, quad):
        if quad == 'ne':
            self.ne.add(envo)
        elif quad == 'nw':
            self.nw.add(envo)
        elif quad == 'se':
            self.se.add(envo)
        else:
            self.sw.add(envo)        

    def add(self, envo):
        """envo must be an EnvObject, may occupy up to 4 quadrants"""
        if len(self.envos) >= MAX_OBJECTS: #If there are too many elems in node
            self.split()            #Create some baby nodes
            for i in self.envos:
                switch = self._which_quad(i)
                self._add_to_quad(i, switch)
            self.envos = []
        elif self.leaf:
            self.envos.append(envo)
            return 
            
        switch = self._which_quad(envo)
        self._add_to_quad(envo, switch)
        #print self.envos
                        

    def print_tree(self):
        if self.leaf:
            print str(self.level) * self.level + str(self.envos)
        else:
            print str(self.level) * self.level
            self.ne.print_tree()
            self.nw.print_tree()
            self.se.print_tree()
            self.sw.print_tree()
        
    def get_node_of(self, envo):
        """precondition that envo is in the tree"""
        if self.leaf:
            return self
        switch = self._which_quad(envo)
        if switch == 'ne':
            return self.ne.get_node_of(envo)
        elif switch == 'nw':
            return self.nw.get_node_of(envo)
        elif switch == 'se':
            return self.se.get_node_of(envo)
        else:
            return self.sw.get_node_of(envo)

    def near_by(self, envo):
        result = []
        node = self.get_node_of(envo)
        x, y = envo.get_pos()
        result.append(node.get_envos())
        #TODO finish this method so it gets neighbouring squares
        #Get squares to the top
        curx = x - 1
        cury = y - 1
        return result


if __name__ == '__main__':
    tree = QuadTree(1, (600,800), 30)
    tree.print_tree()
