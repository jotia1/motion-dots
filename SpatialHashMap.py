import math
from pygame import Rect
from Food import *


class SpatialHashMap(object):
    """A hash of 2D space using the grid size provided"""
    def __init__(self, grid_size, bounding_rect):
        """Create the SpacialHash

        grid_size: Size of each grid square to use
        bounding_rect: The rectangle bounding the area to be mapped. This
                    should be a pygame.Rect
        """
        self.grid = {}  # self.grid = { (x, y):envo) }
        self.key_lookup = {}  # self.key_lookup = { envo:(x, y) }
        if bounding_rect.width % grid_size != 0 or \
                bounding_rect.height % grid_size != 0:
            raise Exception('Rectangle size and grid size incompatible')
        self.border = bounding_rect
        self.grid_size = grid_size


    def _key(self, envo):
        """Given an envo, calculate it's key"""
        x, y = [int(x) for x in envo.get_pos()]
        return (int(math.floor(x / self.grid_size)),
                int(math.floor(y / self.grid_size)))

    def _add_to_grid(self, envo, key):
        if self.grid.has_key(key): #if grid has other envos already
            self.grid.get(key).append(envo) # just append
        else:
            self.grid[key] = [envo]  #make new list

    def _remove_from_grid(self, envo, key):
        """Removes an envo from the grid, key is the grid key"""
        if self.grid.get(key) == None:
            raise Exception("Cannot remove a key that does not exist")
        #print self.grid.get(key)
        if len(self.grid.get(key)) > 1: #if grid has other envos already
            #print self.grid.get(key)
            self.grid.get(key).remove(envo) # just remove it
            #print '---------------'
            #print self.grid.get(key)
        else:
            del self.grid[key] #del whole list
            
    #def _full_remove(self, envo, key):


    def add(self, envo):
        """Add an EnvObject"""
        x,y = envo.get_pos()
        if not self.key_lookup.has_key(envo): #if obj not already in Hash
            key = self._key(envo)            #Get the key for map
            self.key_lookup[envo] = key           #add it to list of objects
            self._add_to_grid(envo, key)     #add it to the map
            return
        
        old_key = self.key_lookup.get(envo)   #if it is in our grid, get old key
        new_key = self._key(envo)       #calc its new key
        #print "keys: ok:{0}, nk{1}, eq{2}".format(old_key, new_key, old_key == new_key)
        if old_key == new_key: #Still in the same grid
            return
        
        #If here we know it has changed grid squares
        self.remove(envo, old_key)  #delete the old entry in grid
        self.key_lookup[envo] = new_key  #Add updated key to keys_lookup
        self._add_to_grid(envo, new_key)        #add at new pos
        


    def remove(self, envo, key=None):
        """Remove an envo and return True or False if envo not here"""
        if not key:
            key = self._key(envo)
        if not self.grid.has_key(key): 
            return False
        self._remove_from_grid(envo, key)
        del self.key_lookup[envo]
        return True

    def get_neighbours(self, envo, dist=1):
        """Get any neighbouring envos that are dist grid squares away"""
        x, y = self._key(envo)
        length = 3 + (dist-1) * 2 #size of the square of grid squares to check
        xleft = x - length/2
        ytop = y - length/2
        envos = []
        for o in xrange(length):
            envos += self.grid.get((xleft + o, ytop), []) #Top row
            envos += self.grid.get((xleft + o, y + length/2), [])#bot row
            envos += self.grid.get((xleft, ytop + o), []) #left side
            envos += self.grid.get((x + length/2, ytop + o), [])#right side
        if dist == 1 or dist == 0:
            envos += self.grid.get((x, y), [])
            while envo in envos:
                envos.remove(envo)
        return list(set(envos))

    def get_closest_envo(self, envo):
        """Return the closest Food or None if no Food in hash"""
        closest = None
        dist = 0
        pos = envo.get_pos()
        while dist * 40 < self.border.width or dist * 40 < self.border.height:
            neighbours = self.get_neighbours(envo, dist)
            for cur in neighbours:
                if not closest:
                    closest = cur
                    continue
                clospos = closest.get_pos()
                curpos = cur.get_pos()
                if self._distance(pos, clospos) > self._distance(pos, curpos):
                    closest = cur
                    
            dist += 1
        return closest
            

    def _distance(self, v1, v2):
        """Calculate the distance between the two 2D vectors"""
        d = (v1[0]-v2[0])**2 + (v1[1]-v2[1])**2
        return math.sqrt(d)

    def get_all(self):
        #print str(len(self.key_lookup.keys())), str(self.key_lookup.keys())
        return self.key_lookup.keys()
                

if __name__ == '__main__':
    from State import *
    from EnvObject import *
    import random
    s = State()
    l = []

    r = Rect(0,0, 800, 600)
    hm = SpatialHashMap(40, r)
    e1 = EnvObject(5, 5, 4, s)
    e2 = EnvObject(10, 10, 4, s)
    e3 = EnvObject(41*5, 41*7, 4, s)
    e4 = EnvObject(41*5, 41*7, 4, s)
    e5 = EnvObject(41*5, 41*7, 4, s)

    hm.add(e1)
    print "One envo added"
    print hm.key_lookup
    print '---------'
    print "\n\n"


    e1.set_pos(40, 5)
    hm.add(e1)
    hm.add(e2)
    print "e1 added again new pos in next square, e2 added"
    print hm.key_lookup
    print '---------'
    print "\n\n"
    hm.remove(e1)
    print "e1 removed"
    print hm.key_lookup
    print "---------"
    
    """
    for i in xrange(10):
        x = random.randint(0, 799)
        y = random.randint(0, 599)
        l.append(EnvObject(x,y,i,s))
    


    
    hm.add(eo)
    for j in l:
        hm.add(j)
    """
 #   hm.add(eo)
    #print hm.objs
    #print hm.map
    #hm.remove(eo)
    print "\n\n\n"
    print "----------------"
    print "FINAL GRID:"
    print hm.grid
    print '----------------'
    print "FINAL KEYS:"
    print hm.key_lookup
    #print [hm._key(x) for x in hm.get_neighbours(eo, 0)]
    print '----------------'
    #print hm.get_closest_envo(eo)
