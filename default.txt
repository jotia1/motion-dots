#Some variables can be defined at runtime
colour_pink = (255, 0, 255)
colour_green =   ( 55, 220 ,100 )
colour_black = (0,0,0)
colour_white = (255,255,255)
colour_red = (240, 20, 20)
colour_interest area = (25, 110, 40 ) 
colour_light green = (0, 255, 0)
 
background colour = black
food colour = light green
food spawn rate = 0.01
seed = 5

#Add characters in the format:
#Character_type, arg1, arg2, arg3, ...
#Some characters will need more or less arguments 
#All arguments are optional, unassigned arguments will be randomly assigned 
#or default values will be used.

#StraightLines takes an x position, y position and starting angle in radians
#StraightLines,colour=white, number=5
#StraightLines

#Brownian, colour= red, number=9

Food, number=3, colour= light green, number=2

#Hungry is a hungry dot that chases the food
Hungry, colour = pink, number= 2

