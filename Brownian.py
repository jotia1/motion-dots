import numpy as np
from Character import *

class Brownian(Character):
    """This Character moves according to Brownian motion
    """

    def __init__(self, x_start, y_start, colour, state):
        """Creates a Character that moves by Brownian motion

        Constructor: BrownianMotion(int, int, int)
        """
        Character.__init__(self, x_start, y_start, colour, state)
        self.speed = 4 #slow down random movement
        self.std = 0.6
        self.next_point = self.generate_point()
        self.counter = 0

    def update(self):
        """Move this character according to Brownian Motion reflecting off walls
         as needed

        BrownianMotion.move() -> None 
        """
        
        """
        #Find components of the vector between new and current points
        dx = np.abs(self.pos[0] - self.next_point[X_AXIS])
        dy = np.abs(self.pos[1] - self.next_point[Y_AXIS])

        #If we are near our target point start moving somewhere else
        if dx < self.size and dy < self.size:
            self.next_point = self.generate_point()

        #Now just move towards the point 
        new_current_dir = np.array([dx, dy])
        new_current_dir /= np.linalg.norm(new_current_dir) #Normalising the vect
        #print "should be 1: " + str(np.linalg.norm(new_current_dir)) #checking
        self.pos[0] += new_current_dir[X_AXIS] + self.speed
        self.pos[1] += new_current_dir[Y_AXIS] + self.speed
        """

        """
        # Brownian motion no editing
        self.pos[0] += np.random.normal(scale=self.std**2) * self.speed
        self.pos[1] += np.random.normal(scale=self.std**2) * self.speed
        """

        #Only update position every second
        self.counter += 1
        if self.counter % 30 == 0:
            self.counter = 0
            self.next_point = self.generate_point()
            self.velocity =  self.next_point - self.pos
            self.velocity /=np.linalg.norm(self.velocity)
            #self.velocity = np.linalg.norm(self.velocity)
            #print "Normalised: " + str(self.velocity)

        self.pos += self.velocity

        #This is done like this as an artifact of the old design
        #Bound to env might get moved up to a super class (e.g. EnvObject)
        #so I was trying to keep generality of the method.
        self.pos = self.bound_to_env(self.pos)

    def generate_point(self):
        """Generate the next set of Brownian co-ordinates

        BrownianMotion.generate_points() -> numpy.ndarray 
        """                         #TODO Check if this is the right return type
        screen = self.state.get_screensize()
        x = np.random.normal(scale=1) * screen[0] + screen[0]/2
        y = np.random.normal(scale=1) * screen[1] + screen[1]/2
        #print "Generated point: " + str(x) + " " + str(y)
        return np.array([x, y], int)

    def __repr__(self):
        x,y = self.get_pos()
        return "Brownian({0}, {1})".format(x, y)


    def bound_to_env(self, pos):
        """Given a position (as a numpy array) if the position is out side
        of the specified environment then will return a point inside the
        environment otherwise will return original point if it is in the env
        area

        Environment.bound_to_env(numpy.array) -> numpy.array
        """
        if pos[0] > self.env.get_width():
            pos[0] = self.env.get_width()
        elif pos[0] < 0:
            pos[0] = 0

        if pos[1] > self.env.get_height():
            pos[1] = self.env.get_height()
        elif pos[1] < 0:
            pos[1] = 0
        return pos
