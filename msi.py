import sys, pygame, math
from pygame.locals import *
import numpy as np
import Tkinter as tk

#Set up pygame elements
SCREEN_SIZE = (800, 500)
FPS = 30

#Set up colours
BACKGROUND =(55,  220, 100)
BLACK =     (0,     0,   0)
WHITE =     (255, 255, 255)
INTEREST =  (25,  110,  40)
BLUE =      (0,    50, 200)
BROWN =     (150, 110,  85)
YELLOW =    (230, 255,   0)

#Other globals
CHARACTER_SIZE = 5      #Character size
GRID_SIZE = CHARACTER_SIZE * 2
GRID_ROWS = int(SCREEN_SIZE[0]/GRID_SIZE)
GRID_COLS = int(SCREEN_SIZE[1]/GRID_SIZE)
IA_SIZE = 5.0
SPEED = 4       #Speed of the less intelligent characters
X_AXIS = 0
Y_AXIS = 1
DEFAULT_FILE = 'default.txt'
#IA_SPAWN_RATE = 0.1


def main():
    """Holds the main loop for the simulation

    main() -> None
    """
    #Set up inital state
    state = State()
    """
    root = tk.Tk()
    root.title('Intro')

    playButton = tk.Button(root, text='Play', command=root.destroy)
    playButton.pack()

    root.mainloop()
    """
    
    #Start pygame/pygame frames per second clock
    pygame.init()
    fpsClock = pygame.time.Clock()
    DISP_SURF = pygame.display.set_mode(SCREEN_SIZE)
    pygame.display.set_caption("Motion of dots")
    
    #Create simulation Environment
    env = Environment(state, SCREEN_SIZE[0], SCREEN_SIZE[1])
    state.set_env(env)
    state.read_file(DEFAULT_FILE)
    np.random.seed(state.get_seed())
    
    #Add a character to the environment
    char = StraightLines(env, 50, 50, math.pi/3)
    char.set_colour((100, 100, 100))
    env.add_character(char)

    #Add random motion character to environment
    rchar = BrownianMotion(env, 250, 250)

    env.add_character(rchar)

    #Add and interestArea
    ia = InterestArea(env, 130, 100)
    env.add_interest_area(ia)

    #Add and interestArea
    ia = InterestArea(env, 730, 400)
    env.add_interest_area(ia)
    
    
    #Add a hungry
    '''
    for i in range(10):
        x = np.random.randint(0, SCREEN_SIZE[0])
        y = np.random.randint(0, SCREEN_SIZE[1])
        hchar1 = Hungry(env, x, y)
        env.add_character(hchar1)
        hchar1.set_colour(YELLOW)
        x = np.random.randint(0, SCREEN_SIZE[0])
        y = np.random.randint(0, SCREEN_SIZE[1])
        ia = InterestArea(env, x, y)
        env.add_interest_area(ia)
    '''
    
    
    
    #Add a hungry
    hchar = Hungry(env, 200, 150)
    env.add_character(hchar)
    hchar.set_colour(YELLOW)
    
        
    while True:
        DISP_SURF.fill(state.get_background_colour())
        
        #Handle closing pygame to stop window freezing
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        env.update(state)       #Update everything, step through time
        env.draw(DISP_SURF)     #Draw the current state of the simulation
        pygame.display.update()
        fpsClock.tick(FPS)

    #state.ask_save_file()

class Environment(object):
    """This will act as the container for character and food patches. 
    """

    def __init__(self, state, x_size, y_size):
        """Takes two ints representing the size of the environment

        Constructor: Environment(State, int, int) -> instance
        """
        self.state = state
        self.size = (x_size, y_size)
        self.characters = []
        self.interest_areas = []
        self.grid = np.array([[None for y in xrange(GRID_COLS)] for x in xrange(GRID_ROWS)] ,object)
        #self.grid = 
                

    def update(self, state):
        """Move all characters within the environment and grow food patches.

        Environment.update(State) -> None
        """
        for character in self.characters:
            character.move() #Update all character positions           
            for ia in self.neighbouringIAs(character):
                dist = self.distance(ia.get_pos(), c.get_pos())
                if dist < ia.get_radius() + c.get_size():
                    number_eating += 1
        '''
        for ia in self.interest_areas:
            number_eating = 0
            for c in self.characters:
                dist = self.distance(ia.get_pos(), c.get_pos())
                if dist < ia.get_radius() + c.get_size():
                    number_eating += 1
        '''
        for ia in self.interest_areas:            
            ia.update(number_eating)
            #Check if Interest Area needs to be removed
            if ia.remove_area():
                self.remove_interest_area(ia)

            #Should this IA spawn a new IA?
            if np.random.random() < state.get_spawn_rate():
                pos = self.new_interest_area(ia)
                if pos != None:
                    self.add_interest_area(InterestArea(self, pos[0], pos[1]))
                


        '''               
        #if all the interest areas have been eaten
        if len(self.interest_areas) == 0:
            self.add_interest_area(InterestArea(self, np.random.randint(0, \
                    self.width()), np.random.randint(0, self.height()) ) )
        '''
        #if we should make another I.A.
        """
        if np.random.random() < IA_SPAWN_RATE:
            index = np.random.randint(0, len(self.interest_areas))
            pos = self.new_interest_area(self.interest_areas[index])
            if pos != None:
                self.add_interest_area(InterestArea(self, pos[0], pos[1]))
        """

    def new_interest_area(self, cur_ia):
        """Return a good position to spawn a new interest area next to the
        supplied interest area or None if random area is not valid.

        Environment.new_interest_area(InterestArea) -> numpy.array
        """

        angle = np.random.uniform(0, 2 * np.pi)
        x, y = cur_ia.get_pos()
        new_pos = np.array([x + 2 * IA_SIZE * np.cos(angle), \
                y + 2 * IA_SIZE * np.sin(angle)])
        for ia in self.neighbouringIAs(cur_ia):
            if ia == cur_ia:
                continue
            if self.distance(new_pos, ia.get_pos()) < 2 * IA_SIZE:
                return None
        return self.bound_to_env(new_pos)

    def neighbouringIAs(self, ia):
        result = []
        x,y = [x-1 for x in ia.get_pos()/GRID_SIZE] #TODO possible index out of range error here...
        #print self.grid[x-1, y-1]
        result.append(self.grid[x - 1, y - 1] if (x > 0 and y > 0) else None)  # Top left
        result.append(self.grid[x, y - 1] if y > 0 else None)                  #Top middle
        result.append(self.grid[x + 1, y - 1] if (x < GRID_ROWS and y > 0) else None)  #Top right
        result.append(self.grid[x - 1, y] if x > 0 else None)  #Left middle
        result.append(self.grid[x, y])                       # Middle Middle
        result.append(self.grid[x + 1, y] if x < GRID_ROWS else None)  # Middle right
        result.append(self.grid[x - 1, y + 1] if x > 0 and y < GRID_SIZE else None)      #bottom left
        result.append(self.grid[x, y + 1] if y < GRID_COLS else None)
        result.append(self.grid[x + 1, y + 1] if y < GRID_SIZE and x < GRID_COLS else None)
        val = []
        for y in [x for x in result if x != None]:
            val += y
        #print val
        return y
    
        


    def set_size(self, x_size, y_size):
        """Setter method for the size of the environment
        
        Environment.set_size(int, int) -> None
        """
        self.size = (x_size, y_size)

    def draw(self, screen):
        """Draw the current state of the simulation to the screen provided

        Environment.draw(pygame.Surface) -> None
        """
        for ia in self.interest_areas:
            pygame.draw.circle(screen, self.state.get_ia_colour(), \
                (ia.get_pos()[X_AXIS], ia.get_pos()[Y_AXIS]), \
                int(ia.get_radius()))

        for c in self.characters:
            pygame.draw.circle(screen, c.get_colour(), (int(c.get_pos()[0]), 
                    int(c.get_pos()[1])), CHARACTER_SIZE) 


    def add_character(self, character):
        """Add a character to the environment
        
        Environment.add_character(Character) -> None
        """
        if character not in self.characters:
            self.characters.append(character)

    def add_interest_area(self, ia):
        """Add the supplied interest area to the environment

        Environment.add_interest_area(InterestArea) -> None
        """
        if ia not in self.interest_areas:
            self.interest_areas.append(ia)
            #add to grid
            x,y = [x-1 for x in ia.get_pos()/GRID_SIZE]
            if self.grid[x, y] == None:
                self.grid[x, y] = [ia]
            else:
                (self.grid[x,y]).append(ia)
                #print self.grid[x, y]
                
                
    def remove_interest_area(self, ia):
        self.interest_areas.remove(ia)
        x,y = [x-1 for x in ia.get_pos()/GRID_SIZE]
        if len(self.grid[x, y]) == 1:
            self.grid[x, y] = None
        else:
            print "removing " + ia
            self.grid[x, y].remove(ia)


    def width(self):
        """Returns the width of the environment
    
        Environment.width() -> int
        """
        return self.size[0]

    def height(self):
        """Returns the height of the environment

        Environment.height() -> int
        """
        return self.size[1]

    def closest_character(self, pos):
        """Return the co-ordinates of the character closest to the 
        supplied point

        Environment(numpy.array) -> numpy.array
        """
        closest = None
        for c in self.characters:
            if closest == None:
                closesnt = c
                continue
            v1 = closest.get_pos()
            v2 = c.get_pos()        
            if self.distance(pos, v2) > self.distance(pos, v1):
                closest = c
        if closest == None or self.distance(closest.get_pos(), pos) < 2:
            return pos
        return closest.get_pos()

    def closest_interest(self, pos):
        """Return the position of the closest Interest Area

        Environment.closest_interest(numpy.array) -> numpy.array
        """
        closest = None
        for ia in self.interest_areas:
            if closest == None:
                closest = ia
                continue
            v1 = closest.get_pos()
            v2 = ia.get_pos()        
            if self.distance(pos, v1) > self.distance(pos, v2):
                closest = ia
        if closest == None or self.distance(closest.get_pos(), pos) < 2:
            return pos
        return closest.get_pos()
        

    def distance(self, v1, v2):
        """Calculate the distance between the two 2D vectors

        Environment.distance(numpy.array, numpy.array) -> int
        """
        d = (v1[0]-v2[0])**2 + (v1[1]-v2[1])**2
        return np.sqrt(d)

    def bound_to_env(self, pos):
        """Given a position (as a numpy array) if the position is out side
        of the specified environment then will return a point inside the
        environment otherwise will return original point if it is in the env
        area

        Environment.bound_to_env(numpy.array) -> numpy.array
        """
        if pos[0] > self.width():
            pos[0] = self.width()
        elif pos[0] < 0:
            pos[0] = 0

        if pos[1] > self.height():
            pos[1] = self.height()
        elif pos[1] < 0:
            pos[1] = 0
        return pos

    def get_interest_areas(self):
        """Returns a reference to the interest areas

        Environment.get_interest_areas() -> list(InterestArea)
        """
        return self.interest_areas

    def get_characters(self):
        """Returns a reference to the list of characters currently in the
        environment

        Environment.get_characters() -> list(Character)
        """
        return self.characters
    

class Character(object):
    """High level class which will group any moving entities in the environment
    """
    
    def __init__(self, env, x_start, y_start):
        """Takes a reference to the environment it is in as well as a 
        starting x and y position along with a starting heading

        Constructor: Character(int, int, int) -> instance
        """
        self.env = env
        self.pos = np.array([x_start, y_start], float)
        self.velocity = np.array([0,0], float)

        #Less essential variables
        self.colour = BLUE
        self.size = CHARACTER_SIZE

    def move(self):
        """To be implemented by subclasses, this will control how this 
        character moves
        
        Character.move() -> None
        """
        print "WARNING: USING MOVE METHOD OF CHARACTER CLASS"
        pass

    def get_pos(self):
        """Return the position of the character

        Character.get_pos() -> numpy.array
        """
        return self.pos

    def set_colour(self, colour):
        """Set this characters colour to the supplied parameter

        Character.set_colour(tuple<int, int, int>) -> None
        """
        self.colour = colour


    def get_colour(self):
        """Returns the colour of this character

        Character.get_colour() -> tuple(int, int, int)
        """
        return self.colour

    def set_size(self, size):
        """Set the size of the character to the size supplied

        Character.set_size(int) -> None
        """
        self.size = size

    def get_size(self):
        """Return the size of a character

        Character.get_size() -> float
        """
        return self.size

    def bound_to_env(self):
        """If the character has moved outside environment boundries will move
        the character back onto an edge.

        Character.bound_to_env() -> None
        """
        self.pos = self.env.bound_to_env(self.pos)
        

class StraightLines(Character):
    """This character moves in straight lines and reflects off walls
    """

    def __init__(self, env, x_start, y_start, r_start):
        """Creates a character that moves in straight lines.
        r_start = 0 represents facing rightwards.

        Constructor: StraightLines(int, int int) -> instance
        """
        Character.__init__(self, env, x_start, y_start)
        self.angle = r_start    #Angle (in radians) to start moving in
        self.speed = SPEED
        self.velocity[X_AXIS] = self.speed * math.cos(self.angle)
        self.velocity[Y_AXIS] = self.speed * math.sin(self.angle)

    def move(self):
        """Move this character in the direction of the current heading or 
        bounces off a wall

        StraightLines.move() -> None
        """
        self.pos += self.velocity

        if self.pos[0] > self.env.width() or self.pos[0] < 0:
            self.velocity[X_AXIS] *= -1

        if self.pos[1] > self.env.height() or self.pos[1] < 0:
            self.velocity[Y_AXIS] *= -1
        

class BrownianMotion(Character):
    """This Character moves according to Brownian motion
    """

    def __init__(self, env, x_start, y_start):
        """Creates a Character that moves by Brownian motion

        Constructor: BrownianMotion(int, int, int)
        """
        Character.__init__(self, env, x_start, y_start)
        self.speed = SPEED #slow down random movement
        self.std = 0.6
        self.next_point = self.generate_point()
        self.counter = 0


    def move(self):
        """Move this character according to Brownian Motion reflecting off walls
         as needed

        BrownianMotion.move() -> None 
        """
        
        """
        #Find components of the vector between new and current points
        dx = np.abs(self.pos[0] - self.next_point[X_AXIS])
        dy = np.abs(self.pos[1] - self.next_point[Y_AXIS])

        #If we are near our target point start moving somewhere else
        if dx < self.size and dy < self.size:
            self.next_point = self.generate_point()

        #Now just move towards the point 
        new_current_dir = np.array([dx, dy])
        new_current_dir /= np.linalg.norm(new_current_dir) #Normalising the vect
        #print "should be 1: " + str(np.linalg.norm(new_current_dir)) #checking
        self.pos[0] += new_current_dir[X_AXIS] + self.speed
        self.pos[1] += new_current_dir[Y_AXIS] + self.speed
        """

        """
        # Brownian motion no editing
        self.pos[0] += np.random.normal(scale=self.std**2) * self.speed
        self.pos[1] += np.random.normal(scale=self.std**2) * self.speed
        """

        #Only update position every second
        self.counter += 1
        if self.counter % 30 == 0:
            self.counter = 0
            self.next_point = self.generate_point()
            self.velocity =  self.next_point - self.pos
            self.velocity /=np.linalg.norm(self.velocity)
            #self.velocity = np.linalg.norm(self.velocity)
            #print "Normalised: " + str(self.velocity)
        
        self.pos += self.velocity

        self.bound_to_env()

    def generate_point(self):
        """Generate the next set of Brownian co-ordinates

        BrownianMotion.generate_points() -> numpy.ndarray 
        """                         #TODO Check if this is the right return type
        x = np.random.normal(scale=1) * SCREEN_SIZE[0] + SCREEN_SIZE[0]/2
        y = np.random.normal(scale=1) * SCREEN_SIZE[1] + SCREEN_SIZE[1]/2
        #print "Generated point: " + str(x) + " " + str(y)
        return np.array([x, y], int)

class Hungry(Character):
    """A character that is very hungry and just goes straight for an interest
    area
    """
    def __init__(self, env, start_x, start_y):
        """Creates a hungry dot

        Constructor: Hungry(Environment, int, int)
        """
        Character.__init__(self, env, start_x, start_y)
        self.next_pos = self.pos

    def calc_closest_food_vect(self):
        pass

    def move(self):
        """Will move towards the closest interest area

        Hungry.move() -> None
        """
        self.next_pos = self.env.closest_interest(self.pos)
        if self.env.distance(self.next_pos, self.pos) < IA_SIZE/2:
            return
        """
        if np.array_equal(self.next_pos,self.pos): #If already on interest area
            return
        """
        
        #Calculate direction of food as unit vector
        magnitude = np.linalg.norm(self.next_pos - self.pos)
        food_vec =(self.next_pos - self.pos) / magnitude

        self.velocity = food_vec # + influence1 + influence2 + ...

        self.pos += self.velocity
        self.bound_to_env()
        

class InterestArea(object):
    """This is an area of the environment in which some characters may prefer 
    to be in or in which they may prefer to avoid.
    """

    def __init__(self, env, x, y):
        """Takes a centerpoint (x and y co-ordinates) and a radius for the area

        Constructor: InterestArea(Environment, int, int)
        """
        #self.env = env
        self.pos = np.array([x, y], int)
        self.radius = 3.0
        self.max_size = IA_SIZE
        self.colour = INTEREST 
        self.growth_rate = 0.07
        self.counter = 1
        self.remove = False 
    
    def get_radius(self):
        """Return the radius of the interest area

        InterestArea.get_radius() -> float
        """
        return self.radius

    def set_colour(colour):
        """sets the colour of the interest item

        InterestArea.set_colour(tuple(int,int,int) -> None
        """
        self.colour = colour

    def get_pos(self):
        """Return the position of the interest area

        InterestArea.get_post() -> numpy.array
        """
        return self.pos

    def set_max(self, num):
        """Set the max size of the interest area

        InterestArea.set_max(int) -> None
        """
        self.max_size = num

    def update(self, num):
        """Responsible for the growth or shrinkage of an interest area
        Takes the number of characters currently influencing it
        
        InterestingArea(int) -> None
        """
        #print self.radius
        if self.radius < 3: #If area is eaten then toggle remove flag
            self.remove = True

        self.growth_rate = -0.1 * num
        if num == 0: #If no one is eating the I.A. then grow
            self.growth_rate = 0.1

        if self.growth_rate > 0 and self.radius < self.max_size:
                self.radius += self.growth_rate
        if self.growth_rate < 0 and  self.radius > 0:
                self.radius += self.growth_rate

    def remove_area(self):
        """Returns True when the area has been eaten and needs to be removed

        InterestArea() -> bool
        """
        return self.remove

    def __repr__(self):
        return "InterestArea({0}, {1})".format(self.pos[0], self.pos[1])

class State(object):

    def __init__(self):
        """A State holds all the state variables for a simulation inclduing:
        - Colours, number and types of characters and locations of I.A.'s

        Constructor: State(Environment)
        """
        self.cur_file = DEFAULT_FILE
        self.valid_chars = ['Hungry', 'StraightLines', 'Brownian']
        self.valid_args = ['x', 'y', 'colour', 'angle']
        self.colours = {}
        self.characters_to_add = []
        self.env = None
        self.vars = {}
        self.reset_state()
    

    def reset_state(self):
        """Returns all state variables back to default values

        State.reset_state() -> None
        """
        self.vars = {'background colour':BACKGROUND, \
                    'interest area colour':INTEREST, \
                    'ia spawn rate':0.1, \
                     'seed':5, \
                    }
        self.cur_file = DEFAULT_FILE
        self.read_file(self.cur_file)

    def is_control_panel_open():
        """Returns True if the control panel is open

        State.control_panel_open() -> bool
        """
        return self.control_panel_open

    def set_env(self, env):
        """Set the environment to be used

        State.set_env(Environment) -> None
        """
        self.env = env
        for char in self.characters_to_add:
            self.env.add_character(char)
        self.characters_to_add = []

    def get_spawn_rate(self):
        """Return the current spawn rate for interest areas

        State.get_spawn_rate() -> float
        """
        return self.vars.get('ia spawn rate')

    def set_control_panel(value):
        """Set the value of whether the control panel is open or not

        State.set_control_panel(bool) -> None
        """
        self.control_panel_open = value
        
    def get_ia_colour(self):
        """Return the RGB value of interest areas as a triple

        State.get_ia_colour() -> tuple(int, int, int)
        """
        return self.interest_area_colour

    def get_background_colour(self):
        """Returns the current colour to be used for the background

        State.get_background_colour() -> tuple(int, int, int)
        """
        return self.background_colour

    def ask_save_file(self):
        """Interacts with user to save the current state of the program to file

        State.ask_save_file() -> None
        """
        pass

    def line_valid_var(self, line):
        """Checks if a given line is a valid variable declaration, that is has
        an equals sign and something before and after that equals sign

        State.line_valid(str) -> bool
        """
        valid = True
        pre, eqs, post = line.partition('=')
        if not eqs or not post.strip():
            valid = False
        if not pre.strip() in self.vars.keys():
            valid = False
        return valid

    def get_characters(self):
        """Return a reference to a list of character

        State.get_characters() -> list(Character)
        """
        return self.characters

    def line_valid_colour_def(self, line):
        """Returns true if the line is a valid colour definition

        State.line_valid_colour_def(str) -> bool
        """
        if not line.startswith('colour_') or not '=' in line:
            return False
        split = line.split('=')
        colour_name = split[0].strip('colour_')
        if not colour_name or len(split) != 2: #if line was 'colour_='
            return False
        return True

    def line_valid_character(self, line):
        """Checks if a given string is a valid character to be created, that is
        it is in the format: Character_type, arg1, arg2, ... where all args
        are valid.

        State.line_valid_character(str) -> bool
        """
        valid = False
        char_name, coma, args = line.partition(',')
        for char in self.valid_chars:
            if char_name == char: #starts with something valid
                valid = True
        if args and valid: #if more then 1 thing in the line
            split = args.split(',')
            for arg in split: #Check all arguments are valid and have '=' signs
                pre, eqs, post = arg.partition('=')
                if not eqs or not post.strip():
                    valid = False
                if pre.strip() not in self.valid_args and \
                       pre.strip() not in self.colours.keys(): 
                    valid = False
        return valid

    def get_seed(self):
        """Return the seed to be used for number generation

        State.get_seed() -> int
        """
        return self.vars.get('seed')
        

    def str_to_colour(self, string):
        """Given a string in format (R, G, B) where R,G,B represent red, green
        and blue components, return a triple with those values

        State.str_to_colour(str) -> tuple(int, int, int)
        """
        split = string.strip('(').strip(')').split(',')
        #print "split is: {0}".format(split)
        if len(split) != 3:
            return None
        try:
            red = int(split[0].strip())
            green = int(split[1].strip())
            blue = int(split[2].strip())
        except ValueError:
            return None
        return (red, green, blue)

    def create_StraightLines(self, name, args):
        """Given strings with the name and arguments of a character
        return an instance of that character. Assume valid args

        State.create_char(str, str) -> Character
        """
        split = args.split(',')
        x = str(np.random.randint(0, SCREEN_SIZE[0]))
        y = str(np.random.randint(0, SCREEN_SIZE[1]))
        colour = str(BLUE)
        for arg in split:
            var, value = [x.strip for x in arg.split('=')]
            x, y = np.random.random(2) * SCREEN_SIZE
            angle = np.random.uniform(0, 2 * np.pi)
            if var == 'x':
                x = value
            if var == 'y':
                y = value
            if var == 'colour':
                colour = value
            if var == 'angle':
                angle = var
        char = eval( "{0}(self.env,{1},{2},{3})".format(name, x, y, angle) )
        char.set_colour(colour)
        return char
            
        

    def read_file(self, filename):
        """Reads in a valid config file and updates the state accordingly

        State.read_file(str) -> None
        """
        fp = open(filename, 'rU')
        error = []
        for i, line in enumerate(fp):
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            if self.line_valid_character(line):
                print line
            if not self.line_valid_var(line) \
                   and not self.line_valid_character(line) \
                   and not self.line_valid_colour_def(line):
                error.append("Could not parse line {0} - {1}".format(i, line))
                continue
            var, eqs, value = line.partition('=')
            var = var.strip()
            value = value.strip()

            if var == 'background colour':
                colour = self.colours.get(value)
                if not colour:
                    error.append("Could not find '{0}' in colour definitions"\
                            .format(value))
                    continue                    
                self.background_colour = colour

            if var == 'interest area colour':
                colour = self.colours.get(value)
                if not colour:
                    error.append("Could not find '{0}' in colour definitions"\
                            .format(value))
                    continue
                self.interest_area_colour = colour
                
            if var == 'seed':
                try:
                    self.vars[var] = int(value)
                except ValueError:
                    error.append("Could not convert {0} to int".format(value))

            if var == 'ia spawn rate':
                try:
                    self.vars[var] = float(value)
                except ValueError:
                    error.append("Could not convert {0} to float".format(value))
                    
            if line.startswith('StraightLines'):
                continue
                #try:
                var,coma,value = [x.strip() for x in line.partition(',')]
                self.characters_to_add.append(self.create_StraightLines(var,\
                        value))
                #except Exception:
                #error.append("Could not create character: {0}".format(var))
                
            if var.startswith('colour_'):
                colour_name = var.partition('colour_')[2]
                colour = self.str_to_colour(value)
                if not colour:
                    error.append("Could not convert '{0}' to a colour".format\
                            (colour_name, value))
                    continue
                self.colours[colour_name] = colour

        for e in error:
            print e
        fp.close()
        

if __name__ == '__main__':
    main()
