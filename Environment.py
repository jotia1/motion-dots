import pygame
from pygame.locals import *
from Food import *

class Environment(object):
    def __init__(self, sizex, sizey, state):
        self.width = sizex
        self.height = sizey
        self.state = state
        self.disp_surf = None
        

    def set_disp_surf(self, disp_surf):
        """Sets the currnet display surface of pygame to use the supplied"""
        self.disp_surf = disp_surf

    def draw(self):
        """Will be responsible for drawing to the canvas

        Environment.draw() -> None
        """
        #self.disp_surf.fill(self.state.get_background_colour())
        for char in self.state.get_characters():
            pygame.draw.circle(self.disp_surf, char.get_colour(),
                    (int(char.get_pos()[0]), int(char.get_pos()[1])),
                    char.get_size())

        for envo in self.state.get_foods():
            pygame.draw.circle(self.disp_surf, envo.get_colour(), \
                (int(envo.get_pos()[0]), int(envo.get_pos()[1])), \
                int(envo.get_size()))            

    def verify_position(self, envo):
        """Given an envo adjust its position if ontop of another envo"""
        if isinstance(envo, Food):
            return
        x, y = envo.get_pos()
        nearby = self.state.get_neighbouring(envo)
        overlapping = True
        while overlapping: #potential issues if we move this envo out of
            overlapping = False #this neighbourhood but this is unlikely
            for i in nearby:
                if self.distance(envo.get_pos(), i.get_pos()) < \
                       envo.get_size() + i.get_size():
                    overlapping = False
                    if x < i.get_pos()[0]: #if our object is to the left
                        envo.set_pos(x - 1, y)
                    else:
                        envo.set_pos(x + 1, y)
            if not overlapping:
                break
            
                
    def get_width(self):
        return self.width

    def get_height(self):
        return self.height
    
    def distance(self, v1, v2):
        """Calculate the distance between the two 2D vectors

        Environment.distance(numpy.array, numpy.array) -> int
        """
        d = (v1[0]-v2[0])**2 + (v1[1]-v2[1])**2
        return np.sqrt(d)
        

    def bound_to_env(self, envo):
        """Keep an envo inside the box of the window"""
        #May get rid of this and just do better error checking
        raise Exception("Shouldn't be calling this")
        pass
