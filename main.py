from State import *
import Tkinter as tk
import sys
import pygame
from pygame.locals import *


def main():
    state = State()

    
    # Start Tkinter stuff
    """
    root = tk.Tk()
    root.title('Intro')

    playButton = tk.Button(root, text='Play', command=root.destroy)
    playButton.pack()

    root.mainloop()
    """
    #Start pygame/pygame frames per second clock
    pygame.init()
    fpsClock = pygame.time.Clock()
    disp_surf = pygame.display.set_mode(state.get_screensize())
    pygame.display.set_caption("Motion dots")
    env = state.get_env()
    env.set_disp_surf(disp_surf)
    
    while True:
        disp_surf.fill(state.get_background_colour())
        
        #Handle closing pygame to stop window freezing
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        state.update()       #Update everything, step through time
        env.draw()     #Draw the current state of the simulation
        pygame.display.update()
        fpsClock.tick(state.get_fps())

if __name__ == '__main__':
    main()
