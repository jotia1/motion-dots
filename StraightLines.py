from Character import *
import math

class StraightLines(Character):
    def __init__(self, x, y, colour, state, angle):
        Character.__init__(self, x, y, colour, state)
        self.angle = angle
        self.speed = state.get_character_speed()
        self.velocity[0] = self.speed * math.cos(self.angle)
        self.velocity[1] = self.speed * math.sin(self.angle)
    
    def update(self):
        """Move this character in a straight line"""
        self.pos += self.velocity

        if self.pos[0] > self.env.get_width() or self.pos[0] < 0:
            self.velocity[0] *= -1

        if self.pos[1] > self.env.get_height() or self.pos[1] < 0:
            self.velocity[1] *= -1


    def __repr__(self):
        return "StrightLines({0}, {1})".format(self.get_pos()[0], self.get_pos()[1])
