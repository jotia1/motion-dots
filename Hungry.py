import numpy as np
from Character import *

class Hungry(Character):
    def __init__(self, x, y, colour, state):
        Character.__init__(self, x, y, colour, state)
        self.next_pos = self.pos
        self.velocity = None

    def update(self):
        """Move hungry!!"""
        closest_food = self.state.closest_food(self)
        if not closest_food:
            return 
        self.next_pos = closest_food.get_pos()
        if self.env.distance(self.next_pos, self.pos) < \
                closest_food.get_size()/2:
            return
        """
        if np.array_equal(self.next_pos,self.pos): #If already on interest area
            return
        """
        
        #Calculate direction of food as unit vector
        magnitude = np.linalg.norm(self.next_pos - self.pos)
        food_vec =(self.next_pos - self.pos) / magnitude

        self.velocity = food_vec # + influence1 + influence2 + ...

        self.pos += self.velocity


    def __repr__(self):
        x, y = self.get_pos()
        return "Hungry({0}, {1})".format(x, y)
