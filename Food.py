from EnvObject import *

class Food(EnvObject):
    def __init__(self, x, y, colour, state):
        EnvObject.__init__(self, x, y, colour, state)
        self.counter = 1
        self.radius = self.state.get_food_start_size()
        self.max_size = self.state.get_food_size()
        self.growth_rate = 0.07
        self.spawn = 0.999


    def update(self):
        """Responsible for the growth or shrinkage of an interest area
        Takes the number of characters currently influencing it
        
        Food(int) -> None
        """
        #print self.radius
        if self.radius < 3: #If area is eaten then toggle remove flag
            self.state.remove_food(self)
            return

        num = self.get_num_eating()
        self.growth_rate = -0.1 * num
        if num == 0: #If no one is eating the I.A. then grow
            self.growth_rate = 0.1

        if self.growth_rate > 0 and self.radius < self.max_size:
                self.radius += self.growth_rate
        if self.growth_rate < 0 and  self.radius > 0:
                self.radius += self.growth_rate

    def get_num_eating(self):
        """Calculate and return the number of characters currently eating"""
        nearby_chars = self.state.get_neighbouring_chars(self)
        count = 0
        for char in nearby_chars:
            if self.distance(char) <= self.radius + char.get_size() + 1:
                count += 1
        return count
                

    def __repr__(self):
        return "Food({0}, {1})".format(self.pos[0], self.pos[1])

    def set_max(self, num):
        """Set the max size of the interest area

        InterestArea.set_max(int) -> None
        """
        self.max_size = num

    def get_size(self):
        """Return the radius of the Food

        Food.get_size() -> float
        """
        return self.radius
