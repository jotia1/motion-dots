import numpy as np
import uuid

class EnvObject(object):
    """A high level object in the simulation can be anything on the board"""
    def __init__(self, x, y, colour, state):
        self.env = state.get_env()
        self.state = state
        self.pos = np.array([x, y], float)
        self.colour = colour
        self.hash = hash(uuid.uuid1().int)

    def set_env(self, env):
        self.env = env

    def update(self):
        """Each object in the environment must update in some way"""
        print "WARNING USING ENVOBJECT UPDATE METHOD"
        
    def get_pos(self):
        """Return the position of the character

        Character.get_pos() -> numpy.array
        """
        return self.pos

    def set_pos(self, x, y):
        """Move the EnvObject to the given location"""
        self.pos[0] = x
        self.pos[1] = y

    def set_size(self):
        """Dummy method for subclasses to fill in"""
        raise Exception("Called Dummy method set_size in EnvObject")

    def set_colour(self, colour):
        """Set this characters colour to the supplied parameter

        Character.set_colour(tuple<int, int, int>) -> None
        """
        self.colour = colour


    def get_colour(self):
        """Returns the colour of this character

        Character.get_colour() -> tuple(int, int, int)
        """
        return self.colour


    def bound_to_env(self):
        """If the character has moved outside environment boundries will move
        the character back onto an edge.

        Character.bound_to_env() -> None
        """
        self.pos = self.env.bound_to_env(self.pos)

    def distance(self, envo):
        """Return the distance this and the supplied envo

        EnvObject.distance(EnvObject) -> float
        """
        vect = self.get_pos() - envo.get_pos()
        return np.sqrt((vect[0])**2 + (vect[1])**2)

    def __repr__(self):
        return "EnvObject({0})".format(self.hash)

    def __hash__(self):
        return self.hash
