import numpy as np
from EnvObject import *

class Character(EnvObject):
    """High level class which will group any moving entities in the environment
    """
    
    def __init__(self, x_start, y_start, colour, state):
        """Takes a reference to the environment it is in as well as a 
        starting x and y position along with a starting heading

        Constructor: Character(int, int, int) -> instance
        """
        EnvObject.__init__(self, x_start, y_start, colour, state)
        self.velocity = np.array([0,0], float)

        #Less essential variables
        self.size = state.get_character_size()

    def update(self):
        """To be implemented by subclasses, this will control how this 
        character moves
        
        Character.update() -> None
        """
        print "WARNING: USING MOVE METHOD OF CHARACTER CLASS"
        pass

    def move(self):
        """Update the position with the velocity"""
        self.pos += self.velocity

    def set_size(self, size):
        """Set the size of the character to the size supplied

        Character.set_size(int) -> None
        """
        self.size = size

    def get_size(self):
        """Return the size of a character

        Character.get_size() -> float
        """
        return self.size

