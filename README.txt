To run this simulation 'pygame' and 'Numpy' will need to be installed.

pygame:
http://www.pygame.org/download.shtml

Numpy:
http://www.numpy.org/

----------------------------------------
To run the simulation execute main.py

----------------------------------------
ISSUES: (in priority order)
- Add in slider boxes adjustments at start of program
    - Extra feature which will allow the program to be configured using slider bars and input fields. This is a Non-urgent feature.
    - Not all values for the state a being loaded from the file in time, e.g. the random number generator seed. Might consider making two files, one for characters and the other for variables. Won't load the character file till variables are loaded and configured in the tk sliders. 
    - Considering abstracting out the task of reading in the variables
- Bugs with character interaction:
    - When characters approach Food horizontally they overlap with it slightly
    - Some characters are gettings stuck when trying to eat food. 
    - Given time might be worth looking into a more intelligent way to resolve how the characters move when they touch. 
- SpacialHashMap effiency might need some improving for large numbers of Hungrys
    - Can handle about 10-15 Hungrys with 60 Food at the moment
    - Can handle up to 20 reasonably but slow down is noticeable
- Fix up commenting

WORKING:
- StraightLines and Brownian seems to be working fine
- Seems to be able to load most sensible commands from a file and update state accordingly. 
- Hungry and Food have been migrated to the new system
- A SpatialHashMap was added in place of the QuadTree which proved difficult to implement from scratch. (SpatialHashMap used instead).
- Food now disappears when eaten (had to fix an issue with spatial hash tree
- Characters no long go through eachother or the food. Slight issues with this.

----------------------------------------
Code inheritance:
                  EnvObject                                 State
                     |---------------------|                  |
                 Character               Food          -------------    
        ---------------------------                    |           |
        |            |            |               Environment   SpatialHashMap
     Brownian     Hungry     StraightLines

----------------------------------------
