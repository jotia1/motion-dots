import sys, random, math
from Environment import *
from Quadtree import *
from Food import *
from SpatialHashMap import *
from pygame import Rect

#Import the characters
from Hungry import *
from StraightLines import *
from Brownian import *


class State(object):

    def __init__(self):
        """A State holds all the state variables for a simulation inclduing:
        - Colours, number and types of characters and locations of I.A.'s

        Constructor: State()
        """
        #Useful constants
        self.vars = {'current file':'default.txt',\
                'SCREEN SIZE X':800, \
                'SCREEN SIZE Y':600,
                'GRID_SIZE':50}
        self.valid_chars = ['Hungry', 'StraightLines', 'Brownian', 'Food']
        self.valid_args = ['x', 'y', 'colour', 'angle', 'number']
        width, height = self.get_screensize()
        grid_size = self.vars.get('GRID_SIZE')
        self.characters = SpatialHashMap(grid_size,
                Rect((0,0), self.get_screensize()))
        self.foods = SpatialHashMap(grid_size,
                Rect((0,0), self.get_screensize()))
        self.colours = {}
        self.env = Environment(width, height, self)

        #Load default file and finish starting up
        self.load_file(self.vars.get('current file'))


    def load_file(self, filename):
        self.vars = {
                'DEFAULT CHAR COLOUR':(0, 50, 200), \
                'SCREEN SIZE X':800, \
                'SCREEN SIZE Y':600, \
                'FPS':30, \
                'background colour':(55,  220, 100), \
                'food colour':(25,  110,  40), \
                'food spawn rate':0.1, \
                'seed':int(random.random()*10), \
                'character size':5, \
                'current file':filename,\
                'character speed':4,
                'max food size':7,
                'food start size':3.0 }
        self.read_file(filename)
        random.seed(self.vars.get('seed'))

    def update(self):
        """Take the state from the current state to the next"""
        for envo in self.get_envos():
            self.update_envo_pos(envo)
            envo.update()
            self.env.verify_position(envo)
            if isinstance(envo, Food):
                self.spawn_food(envo)
            """
            old_pos = envo.get_pos()
            new_pos = 
            nearby = self.foods.get_neighbours(envo) + \
                     self.characters.get_neighbours(envo)
            for i in nearby:
                dist = self.env.distance(new_pos, i.get_pos())
                if dist < envo.get_size() + i.get_size():
                    print "stuff Overlapping"
            """
        #print self.get_envos()
            

    def get_env(self):
        """Returns the current environment

        State.get_env() -> Environment
        """
        return self.env

    def get_food_size(self):
        """Return the max size for food to grow too"""
        return self.vars.get('max food size')

    def remove_food(self, food):
        """Remove the given food from simulation"""
        self.foods.remove(food)
        if len(self.get_foods()) == 0:
            args = self.generate_food_args()
            self.create_food(args)

    def spawn_food(self, food):
        """Given a food decides whether or not to spawn a new food near it"""
        if random.random() < self.get_spawn_rate():
            #self.create_food(self.generate_food_args())#Create and add new food
            
            angle = random.uniform(0, 2 * math.pi)
            x, y = food.get_pos()
            size = self.get_food_size() * 2
            new_pos = np.array([x + ( size * np.cos(angle) ), \
                    y + ( size * np.sin(angle))] )
            gridx, gridy = self.get_screensize()
            if new_pos[0] < 0 or new_pos[1] < 0 or new_pos[0] > gridx \
                    or new_pos[1] > gridy:
                return None #point is outside the screen
            for ia in self.get_neighbouring(food):
                if ia == food:
                    continue
                if self.env.distance(new_pos, ia.get_pos()) < size:
                    return None
            args = {}
            args['colour'] = self.vars.get('food colour')
            args['x'] = new_pos[0]
            args['y'] = new_pos[1]
            self.create_food(args)

    def get_food_start_size(self):
        """Return the size food should be when it starts"""
        return self.vars.get('food start size')

    def get_nearby(self, envo):
        raise Exception("DEPRECATED")
        return self.tree.near_by(envo)

    def update_envo_pos(self, envo):
        if isinstance(envo, Food):
            self.update_food_pos(envo)
        else:
            self.update_char_pos(envo)

    def update_char_pos(self, char):
        """Update the grid positions of characters"""
        self.characters.add(char)

    def update_food_pos(self, food):
        """Update Food position"""
        self.foods.add(food)

    def get_character_speed(self):
        """Return the base speed of the various characters"""
        return self.vars.get('character speed')

    def get_fps(self):
        return self.vars.get('FPS')

    def get_envos(self):
        """Return all EnvObject references including characters and food"""
        return self.characters.get_all() + self.foods.get_all()

    def get_foods(self):
        """Return all instances of food"""
        return self.foods.get_all()

    def get_characters(self):
        """Return the list of all the characters"""
        return self.characters.get_all()

    def get_neighbouring(self, envo):
        """Get all the envo's neighbouring this envo"""
        return self.characters.get_neighbours(envo) + \
               self.foods.get_neighbours(envo)

    def get_neighbouring_chars(self, envo):
        """Return the Characters nearby to envo"""
        return self.characters.get_neighbours(envo)

    def get_neighbouring_food(self, food):
        """Return the Food neighbouring to food"""
        return self.foods.get_neighbours(food)

    def get_screensize(self):
        """Returns the current size of the screen

        State.get_screensize() -> tuple(int, int)
        """
        return (self.vars.get('SCREEN SIZE X'), self.vars.get('SCREEN SIZE Y'))
    
    def get_spawn_rate(self):
        """Return the current spawn rate for interest areas

        State.get_spawn_rate() -> float
        """
        rate = 0.0
        try:
            rate = self.vars.get('food spawn rate')
        except ValueError:
            raise ValueError("Could not convert food" + \
                    "spawn rate to a float: {0}".format(rate))
        return rate

    def get_character_size(self):
        """Return the size of a character

        State.get_character_size() -> int
        """
        return self.vars.get('character size')

    def closest_food(self, envo):
        """Return the Food object closest to envo"""
        return self.foods.get_closest_envo(envo)

    def closest_character(self, envo):
        """Return the Character object closest to envo"""
        return self.characters.get_closest_envo(envo)
        
    def get_food_colour(self):
        """Return the RGB value of interest areas as a triple

        State.get_ia_colour() -> tuple(int, int, int)
        """
        return self.vars.get('food colour')

    def get_background_colour(self):
        """Returns the current colour to be used for the background

        State.get_background_colour() -> tuple(int, int, int)
        """
        return self.vars.get('background colour')

    def get_seed(self):
        """Return the seed to be used for number generation

        State.get_seed() -> int
        """
        return self.vars.get('seed')

    def generate_food_args(self):
        args = {}
        args['x'] = random.randint(0, self.vars.get('SCREEN SIZE X') - 1)
        args['y'] = random.randint(0, self.vars.get('SCREEN SIZE Y') - 1)
        args['colour'] = self.vars.get('food colour')
        return args

    def add_character(self, character):
        """Add a character to the respective list

        State.add_character(Character) -> None
        """
        self.characters.add(character)

    def add_food(self, food):
        """Add a Food object to the state"""
        self.foods.add(food)

    def line_valid_colour(self, line):
        """Returns true if the line is a valid colour definition

        State.line_valid_colour_def(str) -> bool
        """
        if not line.startswith('colour_') or not '=' in line:
            return False
        split = line.split('=')
        colour_name = split[0].strip('colour_')
        if not colour_name or len(split) != 2: #if line was 'colour_='
            return False
        return True

    def line_valid_character(self, line):
        """Checks if a given string is a valid character to be created, that is
        it is in the format: Character_type, arg1, arg2, ... where all args
        are valid.

        State.line_valid_character(str) -> bool
        """
        valid = False
        char_name, coma, args = line.partition(',')
        for char in self.valid_chars:
            if char_name == char: #starts with something valid
                valid = True
        if args and valid: #if more then 1 thing in the line
            split = args.split(',')
            for arg in split: #Check all arguments are valid and have '=' signs
                pre, eqs, post = arg.partition('=')
                if not eqs or not post.strip():
                    valid = False
                if pre.strip() not in self.valid_args and \
                       pre.strip() not in self.colours.keys(): 
                    valid = False
        return valid

    def line_valid_var(self, line):
        """Checks if a given line is a valid variable declaration, that is has
        an equals sign and something before and after that equals sign

        State.line_valid(str) -> bool
        """
        valid = True
        pre, eqs, post = line.partition('=')
        if not eqs or not post.strip():
            valid = False
        if not pre.strip() in self.vars.keys():
            valid = False
        return valid

    def parse_variable(self, line):
        """Given a line with a variable update that variable acordingly
 
        State.parse_variable(str) -> None
        """
        var, eqs, value = line.partition('=')
        var = var.strip()
        value = value.strip()
        
        if var == 'background colour' or var == 'food colour':
            colour = self.colours.get(value)
            if not colour:
                raise InvalidLine("Could not find a previous definition for " \
                        + "colour {0}. Ensure {0} is defined.".format(value))                   
            self.vars[var] = colour
        """
        if var == 'interest area colour':
            colour = self.colours.get(value)
            if not colour:
                raise InvalidLine("Could not find '{0}' in colour definitions"\
                        .format(value))
            self.vars['food colour'] = colour
        """
            
        if var == 'seed':
            try:
                self.vars['seed'] = int(value)
                random.seed(value)
            except ValueError:
                raise InvalidLine("Could not convert {0} to int".format(value))

        if var == 'food spawn rate':
            try:
                self.vars['food spawn rate'] = float(value)
            except ValueError:
                raise InvalidLine("Could not convert {0} to float"\
                        .format(value))

    def parse_colour(self, line):
        var, eqs, value = line.partition('=')
        var = var.strip()
        value = value.strip()
        colour_name = var.partition('colour_')[2]
        colour = self.str_to_colour(value)
        if not colour:
            raise InvalidLine("Could not convert '{0}' to a colour".format\
                    (value))
        self.colours[colour_name] = colour

    def parse_character(self, line):
        split = [x.strip() for x in line.split(',')]
        name = split[0]
        args_list = split[1:]
        args = {}
        randx = False
        randy = False
        for i in args_list: #Create a list of all the arguments
            var, eqs, value = [x.strip() for x in i.partition('=')]
            args[var] = value

        try:
            if args.get('x') == None: #if no x param then generate rand
                randx = True
            else:
                args['x'] = int(args.get('x'))
            #If no y param
            if args.get('y') == None:
                randy = True
            else:
                args['y'] = int(args.get('y'))
            #if number to create is not specified
            if args.get('number') == None:
                args['number'] = 1
            else:
                args['number'] = int(args.get('number'))
        except ValueError:
            raise InvalidLine("Cannot parse x and y parameters")
        
        if args.get('colour') == None:
            args['colour'] = self.vars.get('DEFAULT CHAR COLOUR')
        else:
            colour = None
            if args.get('colour') in self.colours:
                colour = self.colours.get(args.get('colour'))
            if not colour:
                raise InvalidLine("Cannot load {0} as colour"\
                        .format(args.get('colour')))
            args['colour'] = colour


        #now args are generated lets create the character
        count = 0
        while count < args.get('number'):
            if randx:
                args['x'] = random.randint(0,
                        self.vars.get('SCREEN SIZE X') - 1)
            if randy:
                args['y'] = random.randint(0,
                        self.vars.get('SCREEN SIZE Y') - 1)               
            if name == 'StraightLines':
                self.create_straightlines(args)
            if name == 'Hungry':
                self.create_hungry(args)
            if name == 'Brownian':
                self.create_brownian(args)
            if name == 'Food':
                self.create_food(args)
            count += 1

    def create_hungry(self, args):
        x = args.get('x')
        y = args.get('y')
        colour = args.get('colour')
        self.add_character(Hungry(x, y, colour, self))

    def create_food(self, args):
        x = args.get('x')
        y = args.get('y')
        colour = args.get('colour')
        self.add_food(Food(x, y, colour, self))

    def create_straightlines(self, args):
        angle = args.get('angle')
        if angle == None:
            angle = random.random() * 2 * math.pi
        x = args.get('x')
        y = args.get('y')
        colour = args.get('colour')
        self.add_character(StraightLines(x, y, colour, self, angle))

    def create_brownian(self, args):
        x = args.get('x')
        y = args.get('y')
        colour = args.get('colour')
        self.add_character(Brownian(x, y, colour, self))


    def str_to_colour(self, string):
        """Given a string in format (R, G, B) where R,G,B represent red, green
        and blue components, return a triple with those values

        State.str_to_colour(str) -> tuple(int, int, int)
        """
        split = string.strip('(').strip(')').split(',')
        #print "split is: {0}".format(split)
        if len(split) != 3:
            return None
        try:
            red = int(split[0].strip())
            green = int(split[1].strip())
            blue = int(split[2].strip())
        except ValueError:
            return None
        return (red, green, blue)

    def read_file(self, filename):
        """Reads in a valid config file and updates the state accordingly

        State.read_file(str) -> None
        """
        fp = open(filename, 'rU')
        if fp == None:
            print "Could not open file: " + filename
            return None
        error = []
        for i, line in enumerate(fp):
            line = line.strip()
            if line == '' or line.startswith('#'):
                continue
            if self.line_valid_var(line):
                self.parse_variable(line)
            elif self.line_valid_character(line):
                self.parse_character(line)
            elif self.line_valid_colour(line):
                self.parse_colour(line)
            else:
                raise InvalidLine("Line {0} not valid - {1}".format(i, \
                        line))
        for e in error:
            print e
        fp.close()

class InvalidLine(Exception):
    """An exception when a line in a file is not a valid input for state"""
    def __init__(self, message):
        """Creates an exception when a line cannot be parsed from a file

        Constructor: InvalidLine(str)
        """
        Exception.__init__(self, message)

if __name__ == '__main__':
    s = State()
